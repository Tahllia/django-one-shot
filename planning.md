**Feature 1**
*[x]Fork and clone the starter project from django-one-shot 
*[x]Create a new virtual environment in the repository directory for the project
    *[x]python -m  venv .venv
*[x]Activate the virtual environment
    *[x].\.venv\Scripts\Activate.ps1   
*[x]Upgrade pip
    *[x]python -m pip install --upgrade pip
*[x]Install django
    *[x]pip install django
*[x]Install black
    *[x]pip install black 
*[x]Install flake8
    *[x]pip install flake8
*[x]Install djlint
    *[x]pip install djlint
*[x]Install django debug toolbar
    *[x]python -m pip install django-debug-toolbar
        *[x]see this link: https://django-debug-toolbar.readthedocs.io/en/latest/installation.html 
*[x]Deactivate your virtual environment
    *[x]deactivate
*[x]Activate your virtual environment
    *[x].\.venv\Scripts\Activate.ps1
*[x]Use pip freeze to generate a requirements.txt file
    *[x]python -m pip freeze > requirements.txt

**Feature 2**
*[x]Create a Django project named brain_two so that the manage.py file is in the top directory
    *[x]django-admin startproject brain_two .
*[x]Create a Django app named todos and install it in the brain_two Django project in the INSTALLED_APPS list
    *[x]python manage.py startapp todos 
    *[x]INSTALLED_APPS = ["todos.apps.TodosConfig", ...
*[x]Run the migrations
    *[x]python manage.py makemigrations
    *[x]python manage.py migrate
*[x]Create a super user
    *[x]python manage.py createsuperuser

** Feature 3
*[x] Create a TodoList model in the todos Django app
    *[x]in models.py create the class
    **Feature 4
    *[x] in admin.py we need to register the model
        *[x]from .models import <name of the class>
        *[x] admin.site.register(<name of the class>)

**Feature 5
*[x]add a new model named TodoItem
    *[x]add task
    *[x]add due_date
    *[x]add is_completed
    *[x]add list as a foregienkey 
*[x]Feature 6
    *[x]Register the TodoItem model with the admin so that you can see it in the Django admin site.

    **Feature 7
*[x]Create a view that will get all of the instances of the TodoList model and put them in the context for the template.
*[xRegister that view in the todos app (todos/urls.py) for the path "" and the name "todo_list_list" in a new file named todos/urls.py.
    *[x]path("", TodoListView.as_view(), name="todo_list_list"),
*[x]Include the URL patterns from the todos app in the brain_two project with the prefix "todos/".
    *[x]path("todos/", include("todos.urls")),
*[x]Create a template for the list view that complies with the  specifications.

**Feature 8
*[x]Create a view that shows the details of a particular to-do list, including its tasks
*[x]In the todos urls.py file, register the view with the path "<int:pk>/" and the name "todo_list_detail"
*[x]Create a template to show the details of the todolist and a table of its to-do items
*[x]Update the list template to show the number of to-do items for a to-do list
*[x]Update the list template to have a link from the to-do list name to the detail view for that to-do list

**Feature 9
*[x]Create a create view for the TodoList model that will show the name field in the form and handle the form submission to create a new TodoList
*[x]If the to-do list is successfully created, it should redirect to the detail page for that to-do list
*[x]Register that view for the path "create/" in the todos urls.py and the name "todo_list_create"
*[x]Create an HTML template that shows the form to create a new TodoList (see the template specifications below)
*[x]Add a link to the list view for the TodoList that navigates to the new create view

**Feature 10
*[]Create an update view for the TodoList model that will show the name. field in the form and handle the form submission to change an existing TodoList
*[x]If the to-do list is successfully edited, it should redirect to the detail page for that to-do list.
*[x]Register that view for the path /todos/<int:pk>/edit in the todos urls.py and the name "todo_list_update".
*[x]Create an HTML template that shows the form to edit a new TodoList (see the template specifications below).
*[x]Add a link to the list view for the TodoList that navigates to the new update view.

**Feature 11
*[x]Create a delete view for the TodoList model that will show a delete button and handle the form submission to delete an existing TodoList.
*[x]If the to-do list is successfully deleted, it should redirect to the to-do list list view.
*[x]Register that view for the path /todos/<int:pk>/delete in the todos urls.py and the name "todo_list_delete".
*[x]Create an HTML template that shows the form to delete a new TodoList (see the template specifications below).
*[x]Add a link to the detail view for the TodoList that navigates to the new delete view.

**Feature 12
*[x]Create a create view for the TodoItem model that will show the task field in the form and handle the form submission to create a new TodoItem.
*[x]If the to-do list is successfully created, it should redirect to the detail page for that to-do list.
*[x]Register that view for the path "items/create/" in the todos urls.py and the name "todo_item_create".
*[x]Create an HTML template that shows the form to create a new TodoItem (see the template specifications below).
*[x]Add a link to the list view for the TodoList that navigates to the new create view.

**Feature 13
*[]Create an update view for the TodoItem model that will show the task field, the due date field, an is_completed checkbox, and a select list showing the to-do lists in the form and handle the form submission to change an existing TodoItem.
*[]If the to-do item is successfully edited, it should redirect to the detail page for the to-do list.
*[]Register that view for the path /todos/items/<int:pk>/edit in the todos urls.py and the name "todo_item_update".
*[]Create an HTML template that shows the form to edit a TodoItem (see the template specifications below).
*[]Add a link to the list view for the TodoList that navigates to the new update view.
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]









**************
****Feature 8
*[]