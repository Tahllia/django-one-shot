from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.shortcuts import redirect, render

from todos.models import TodoItem, TodoList

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "whaetver"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    context_object_name = "create"

    def get_success_url(self):  # look into get success url
        return reverse_lazy("todo_list_detail", kwargs={"pk": self.object.id})


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def get_success_url(self):  # look into get success url
        return reverse_lazy("todo_list_detail", kwargs={"pk": self.object.id})


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")

    # def get_success_url(self):  # look into get success url
    #     return reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/item_create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/item_update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):  # look into get success url
        return reverse_lazy(
            "todo_list_detail", kwargs={"pk": self.object.list.id}
        )
